mkdir -p cdroot-ppc64/boot

if ! type grub-mkimage>/dev/null; then
	printf "GRUB image cannot be created.  Using stale copy.\n"
	printf "If you don't have one, this will fail!\n"
else
	grub-mkimage -c ppc/early.cfg64 -v -p boot -o cdroot-ppc64/boot/grubcore.img -O powerpc-ieee1275 boot btrfs datetime disk ext2 gfxmenu help hfs hfsplus ieee1275_fb iso9660 jfs ls luks lvm macbless macho nilfs2 ofnet part_apple part_gpt part_msdos png scsi search xfs linux reboot gfxterm gfxterm_background gfxterm_menu
fi

cp AdelieTux.icns 'cdroot-ppc64/Icon'
cp ppc/grub.cfg64 cdroot-ppc64/boot/grub.cfg
cp ppc/ofboot.b cdroot-ppc64/boot/ofboot.b
cp cdroot-ppc64/boot/ofboot.b cdroot-ppc64/boot/bootinfo.txt

# Fix petitboot
mkdir cdroot-ppc64/boot/grub
cp ppc/grub.cfg64 cdroot-ppc64/boot/grub/grub.cfg

# Fix CHRP
mkdir cdroot-ppc64/ppc
cp cdroot-ppc64/boot/bootinfo.txt cdroot-ppc64/ppc/bootinfo.txt
