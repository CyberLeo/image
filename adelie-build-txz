#!/bin/sh

set -e
def_arch=$(uname -m)
def_ver="1.0-rc1"
readonly PROGNAME=$(basename $0)


[ -z $ZSH_VERSION ] || set -y


warn() {
    printf '>>> '
    printf '\033[01;33mWARNING\033[00;39m '
    printf '>>> '
    printf '\033[01;33mWARNING\033[00;39m '
    printf '>>> '
    printf '\033[01;33mWARNING\033[00;39m '
    printf '>>> '
    printf '\033[01;33mWARNING\033[00;39m '
    printf '>>>\n'
}


fatal() {
    printf '>>> '
    printf '\033[01;31mERROR\033[00;39m '
    printf '>>> '
    printf '\033[01;31mERROR\033[00;39m '
    printf '>>> '
    printf '\033[01;31mERROR\033[00;39m '
    printf '>>> '
    printf '\033[01;31mERROR\033[00;39m '
    printf '>>>\n'
}


ensure_commands() {
    if ! command -v apk>/dev/null 2>/dev/null; then
        fatal
        printf 'You must have apk installed.  On Gentoo, see sys-devel/apk-tools.\n'
        exit 127
    fi

    if ! command -pv tar>/dev/null 2>/dev/null; then
        fatal
        printf 'You must have tar installed.\n'
        exit 127
    fi
}


usage() {
    printf 'usage: %s [-a ARCH] [--mini] [-v VERSION] [--help]\n\n' $PROGNAME
    printf 'Create an Adélie Linux root FS tarball (.TXZ) using the specified parameters.\n\n'
    printf 'Default ARCH: %s\n' $def_arch
    printf 'Default VERSION: %s\n' $def_ver
}


while [ -n "$1" ]; do
    case $1 in
        -a | --arch)
            shift
            readonly MY_ARCH=$1
            ;;
        -h | --help)
            usage
            exit
            ;;
	--mini)
	    readonly MINI=yes
	    ;;
        -v | --version)
            shift
            readonly MY_VER=$1
            ;;
        *)
            usage >&2
            exit 127
            ;;
    esac
    shift
done

set -a
readonly ARCH=${MY_ARCH:-$def_arch}
readonly LDARCH=${LDARCH:-$ARCH}
readonly PHASE=${MY_PHASE:-all}
readonly VERSION=${MY_VER:-$def_ver}
readonly APKVER=${MY_APKVER:-$VERSION}
readonly URL=${MY_URL:-https://distfiles.adelielinux.org/adelie/$VERSION/}
set +a

ensure_commands

header() {
	printf '\033[01;32m * \033[37m%s\033[00;39m\n' "$1"
}

header 'Adélie Linux Root FS Tarball Creation Tool'
printf '\n'

clean_dirs() {
	warn
	printf 'This will erase all files at the directories %s/rootfs-%s.\n\n' `pwd` $ARCH
	printf 'When you are ready, press RETURN.  To cancel, press Ctrl-C.\n'
	read dontcare

	rm -rf rootfs-$ARCH
	mkdir rootfs-$ARCH
	mkdir -p out
}

install_pkgs() {
	header "Installing base system to tar root..."

	if test -n "${MINI+mini}"; then
		readonly PACKAGES="adelie-core dash-binsh"
		readonly ARCH_PKGS=""
	else
		readonly PACKAGES="$(cat packages/base 2>/dev/null | tr '\n' ' ' || fatal 'No core packages specified')"
		readonly ARCH_PKGS="$(cat packages/arch/$ARCH 2>/dev/null | tr '\n' ' ' || echo '')"
	fi

	mkdir -p rootfs-$ARCH/etc/apk
	cp -r keys rootfs-$ARCH/etc/apk/
	# Disable grub trigger.
	mkdir -p rootfs-$ARCH/etc/default
	printf "ADELIE_MANUAL_CONFIG=1\n" >> rootfs-$ARCH/etc/default/grub
	mkdir -p rootfs-$ARCH/dev
	mknod rootfs-$ARCH/dev/urandom c 1 9
	mkdir -p rootfs-$ARCH/usr/sbin
	apk --arch $ARCH \
		-X "$URL/system/$EXTRA_MIRROR" \
		-X "$URL/user/$EXTRA_MIRROR" \
		-U --root rootfs-$ARCH --initdb add $PACKAGES $ARCH_PKGS
}

make_structure() {
	if test -n "${MINI+mini}"; then
		echo 'adelie-mini' > rootfs-$ARCH/etc/hostname
	else
		echo 'adelie-root' > rootfs-$ARCH/etc/hostname

		for _runlevel in sysinit boot default shutdown; do
			mkdir -p rootfs-$ARCH/etc/runlevels/$_runlevel
		done

		echo 'mtab_is_file=no' > rootfs-$ARCH/etc/conf.d/mtab

		for siservice in udev udev-trigger lvmetad; do
			ln -s /etc/init.d/$siservice \
			   rootfs-$ARCH/etc/runlevels/sysinit/$siservice
		done

		for bootservice in root binfmt bootmisc fsck hostname hwclock \
		    keymaps localmount loopback modules mtab procfs sysctl \
		    sysfsconf termencoding tmpfiles.setup urandom; do
			ln -s /etc/init.d/$bootservice \
			   rootfs-$ARCH/etc/runlevels/boot/$bootservice
		done

		readonly BASE_SERVICES="$(cat services/base 2>/dev/null | tr '\n' ' ' || echo '')"

		[ -z BASE_SERVICES ] || for base_service in $BASE_SERVICES; do
			ln -s /etc/init.d/$base_service \
			    rootfs-$ARCH/etc/runlevels/default/$base_service
		done

		cp AdelieTux.icns rootfs-$ARCH/.VolumeIcon.icns

		# Put a copy of the kernel(s) in the kernels/$ARCH/ directory,
		# so users may download them for netbooting or such.
		mkdir -p out/kernels/$ARCH
		cp -r rootfs-$ARCH/boot/* out/kernels/$ARCH/
	fi

	cat >rootfs-$ARCH/etc/fstab <<- FSTAB
		# Welcome to Adélie Linux.
		# This fstab(5) is for the live media only.  Edit for use for your installation.

		tmpfs	/tmp		tmpfs	defaults	0	1
		proc	/proc		proc	defaults	0	1
	FSTAB

	cat >rootfs-$ARCH/etc/resolv.conf <<- RESOLVE
		nameserver 2620:fe::fe
		nameserver 9.9.9.9
		nameserver 149.112.112.112
	RESOLVE

	cat >rootfs-$ARCH/etc/apk/repositories <<-REPOS
		https://distfiles.adelielinux.org/adelie/$APKVER/system/$EXTRA_MIRROR
		https://distfiles.adelielinux.org/adelie/$APKVER/user/$EXTRA_MIRROR
	REPOS
}

tar_it() {
	header 'Creating file system archive...'

	cd rootfs-$ARCH
	tar -cf - . | xz -v > ../out/adelie-rootfs-${MINI+mini-}$ARCH-$VERSION-$(date +%Y%m%d).txz
}

# in case we want to add phase support like adelie-build-cd has later
case $PHASE in
	clean)
		clean_dirs
		;;
	install)
		install_pkgs
		make_structure
		;;
	tar)
		tar_it
		;;
	all)
		clean_dirs
		install_pkgs
		make_structure
		tar_it
		;;
	*)
		fatal
		printf 'Unknown phase %s.  Stop.\n' $PHASE
		;;
esac
